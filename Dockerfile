FROM node:alpine as node

WORKDIR /demo-thinklink

COPY package*.json .
RUN npm install

COPY . .

EXPOSE 8080

CMD ["npm", "run", "start"]
