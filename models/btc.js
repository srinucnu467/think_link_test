module.exports = function(sequelize, Sequalize) {
    var BtcSchema = sequelize.define("BtcPrice", {
        price: Sequalize.INTEGER,
        inserted_at:Sequalize.STRING,
        created_at:Sequalize.INTEGER
    },{
        timestamps: false
    });
    return BtcSchema;
}