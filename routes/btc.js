var express = require('express');
var BtcPrice = require('../models').BtcPrice;
var router = express.Router();
const axios = require('axios');
var nodemailer = require('nodemailer');
var transport = nodemailer.createTransport({
    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT,
    auth: {
      user: process.env.MAIL_USER,
      pass: process.env.MAIL_PASS
    }
  });

// default method
router.get('/', function(req, res){
    setInterval(function(){
        getPriceandInsert()}, 30000) //interval in milliseconds 30 sec
});

function getPriceandInsert(){
    //api call
 axios.get('https://api.coingecko.com/api/v3/simple/price?ids=bitcoin&vs_currencies=USD')
  .then(function (response) {
    // handle success
    var data = response.data; //price data
    console.log(process.env.MIN_VAL);
    //comapre user input value and send alert mail
    if(parseInt(data['bitcoin']['usd']) > parseInt(process.env.MAX_VAL)){
        sendEmail(process.env.MAX_ALERT_MESSAGE);
        console.log(process.env.MAX_ALERT_MESSAGE);
    }
    if(parseInt(data['bitcoin']['usd']) < parseInt(process.env.MIN_VAL)){
        sendEmail(process.env.MIN_ALERT_MESSAGE);
        console.log(process.env.MIN_ALERT_MESSAGE);
    }
    //data insertion
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = dd + '-' + mm + '-' + yyyy;

    BtcPrice.create({
        price: data['bitcoin']['usd'],
        inserted_at:today,
        created_at: Math.floor(Date.now() / 1000)
    }).then(btcprice => {
        console.log("inserted");
    }).error(err => {
        res.status(405).json('Error has occured');
    });
  })
  .catch(function (error) {
    // handle error
    console.log(error);
  })
}
//send email
function sendEmail(message){
    var mailOptions = {
        from: '"Srinu Korakana" <srinucnu467@gmail.com>',
        to: process.env.USER_EMAIL,
        subject: 'Sample Test Mail',
        html: '<b>'+message+'</b>',
    };
    transport.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message sent: %s', info.messageId);
    });
}

//query method
router.get('/data', function(req, res){
    var date = req.query.date;
    var offset = req.query.offset;
    var limit = req.query.limit;
    BtcPrice.findAll({
        where: {inserted_at: date},
        limit : [offset , limit]
        
    }).then(btc => {
        const data = new Object();
        data['count'] = btc.length;
        
        var response_data = [];
        for (let i = 0; i < btc.length; i++)  {
            var time_stamp = new Date(btc[i].dataValues.created_at * 1000).toISOString();
            response_data.push({"timestamp":time_stamp,"price":btc[i].dataValues.price,"coin": "btc"});
        }
        data['data'] = response_data;
        res.status(200).json(data);
    });
});



module.exports = router;